package com.example.hw_002.model;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Article {
    private int id;
    private String title;
    private String description;
    private String action;
    private String profile;
    private MultipartFile file;
}
