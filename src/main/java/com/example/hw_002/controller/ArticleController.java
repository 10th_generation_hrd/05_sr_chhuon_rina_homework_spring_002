package com.example.hw_002.controller;

import com.example.hw_002.model.Article;
import com.example.hw_002.service.ArticleService;
import com.example.hw_002.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
public class ArticleController {
    @Autowired
    @Qualifier("articleServiceImp")
    ArticleService articleService;

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping
    public String index(Model model){
        model.addAttribute("article", new Article());
        model.addAttribute("articles",articleService.getAllArticles());
        return "index";
    }
    @GetMapping("form-add")
    public String showFormAdd(Model model){
        model.addAttribute("article", new Article());
        return "form-add";
    }
    @PostMapping("/handle-add")
    public String handleAdd(@ModelAttribute Article article,BindingResult bindingResult){
        try{
            String filename = "http://localhost:8080/images/"+ fileStorageService.saveFile(article.getFile());
            System.out.println("filename :" + filename);
            article.setProfile(filename);
        }catch (IOException ex){
            System.out.println("Error with the image upload"+ex.getMessage());
        }

        if(bindingResult.hasErrors()){
            return "/form-add";
        }
        System.out.println("Article : " + article);
        articleService.AddArticleMethod(article);
        return "redirect:/";
    }
    @GetMapping("/form-view/{id}")
    public String viewArticle(@PathVariable int id, Model model){
        Article resultArticle = articleService.findArticleByID(id);
        System.out.println("result article : "+ resultArticle);
        model.addAttribute("article",resultArticle);
        return "form-view";
    }
    @GetMapping("form-delete/{id}")
    public String deleteArticle(@PathVariable int id ){
        System.out.println(id);
        articleService.deleteArticleByID(id);
        return "redirect:/";
    }


    @GetMapping("/form-update/{id}")
    public String updateArticle(Model model, @PathVariable int id){

        Article article= articleService.findArticleByID(id);

        System.out.println("Here is the value of editable article "+ article);
        //Article article= articleService.getAll().get(0);
        model.addAttribute("article",article);

        return "form-update";
    }



// update Record operation @PostMapping("{/update-article")
//@GetMapping("update-article")

    @PostMapping("/update")
    public String updateHandler( @ModelAttribute Article article){

        String  updateImageUrl= "";
        try{
            if (article.getFile().isEmpty()){
                updateImageUrl= article.getProfile();
            } else {


                updateImageUrl="http://localhost:8080/images/"+fileStorageService.saveFile(article.getFile());


            }

        }catch (IOException exception){

            System.out.println(" something's wrong with the file ");
        }

        article.setProfile(updateImageUrl);
        articleService.updateArticle(article.getId(),article);


        System.out.println("Updated article is :   "+article);
        return  "redirect:/";

    }




}
