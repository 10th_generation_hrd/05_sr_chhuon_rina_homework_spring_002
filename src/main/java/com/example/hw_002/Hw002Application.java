package com.example.hw_002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hw002Application {

    public static void main(String[] args) {
        SpringApplication.run(Hw002Application.class, args);
    }

}
