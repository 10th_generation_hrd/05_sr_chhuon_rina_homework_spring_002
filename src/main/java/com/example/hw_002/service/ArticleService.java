package com.example.hw_002.service;

import com.example.hw_002.model.Article;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface ArticleService {

    public List<Article>getAllArticles();
    public Article findArticleByID(int id);
    public void studentMethod();
    public void AddArticleMethod(Article article);
    public boolean deleteArticleByID(int id);
    public void updateArticle(int id,Article article);
}
