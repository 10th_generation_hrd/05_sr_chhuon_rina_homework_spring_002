package com.example.hw_002.service.serverImp;

import com.example.hw_002.model.Article;
import com.example.hw_002.responsitoty.ArticleRepository;
import com.example.hw_002.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {
    @Autowired
    ArticleRepository articleRepository;


    @Override
    public List<Article> getAllArticles() {
        return articleRepository.getAllArticles();
    }

    @Override
    public Article findArticleByID(int id) {
        return articleRepository.findArticleByID(id);
    }

    @Override
    public void studentMethod() {

    }

    @Override
    public void AddArticleMethod(Article article) {
//        if (article.getId()==0){
//            System.out.println(" Insert to database false");
//        }
//        else {
//            articleRepository.addNewArticle(article);
//        }
        article.setId(articleRepository.getAllArticles().size()+1);
        articleRepository.addNewArticle(article);
    }

    @Override
    public boolean deleteArticleByID(int id) {
        boolean result = articleRepository.deleteArticleByID(id);
        if (result){
            System.out.println("Delete Success!!!");
            return true;
        }else {
            System.out.println("Delete Unsuccess!!!");
            return false;
        }
    }

    @Override
    public void updateArticle(int id,Article article) {
        articleRepository.updateArticle(id, article);
    }
}
