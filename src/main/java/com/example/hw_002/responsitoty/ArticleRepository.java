package com.example.hw_002.responsitoty;

import com.example.hw_002.model.Article;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ArticleRepository {
    List<Article> articles = new ArrayList<>();


    public ArticleRepository(){
        int i = 0;
            Article article = new Article();
            article.setId(i+1);
            article.setTitle("Khmer");
            article.setDescription("The Movie Title: Directed by Jake Reed, Lucy Rose. With Michael Adamson, Zoe Birkbeck, Catherine Catizone, Luke Cowens.");
            article.setProfile("http://localhost:8081/images/9eb38499-d987-4b2f-9462-8ab9f328945e.JPG");
            articles.add(article);

    }
    public List<Article>getArticles(){ return articles;}
    public List<Article>getAllArticles(){
        return getArticles();
    }
    public void addNewArticle(Article article){
        article.setId(articles.size()+1);
        articles.add(article);
    }
    public boolean deleteArticleByID(int id){
        boolean status = true;
        for (int i = 0; i < articles.size(); i++) {
            if(id==articles.get(i).getId()){
                articles.remove(articles.get(i));
                status = true;
            }
            else{
                status = false;
            }
        }
        return status;
    }
//    public void updateArticleByID(int id){
//        int index=-3;
//        for (int i = 0; i < articles.size(); i++) {
////            if (id==articles.get(i).getId()){
////                articles.set(id,articles);
////            }
//            if(articles.get(i).getId()== id ){
//
//                index = i;
//                break;
//            }
//            articles.set(index,Article);
//            return true;
//        }
//    }

    public Article findArticleByID(int id){
        return articles.stream().filter(article -> article.getId()==id).findFirst().orElseThrow();
    }

    public boolean updateArticle( int id , Article article){


        // using the ID to get the index
        int index=-3 ;
        for ( int i=0 ; i< articles.size(); i++){

            if(articles.get(i).getId()== id ){

                index = i;
                break;
            }

        }
        articles.set(index,article);
        return true;
    }


}
